from django import forms
from .models import Akun
class Register_Form(forms.ModelForm):
	class Meta:
		widgets= {'password': forms.PasswordInput(), 'confirm_password': forms.PasswordInput()}
		model = Akun
		fields = [
			'nama_akun',
			'email_akun',
			'dob',
			'password',
		]
