from django.db import models
class Akun(models.Model):
	nama_akun = models.CharField(max_length=32)
	email_akun = models.EmailField(max_length=64)
	dob = models.DateField(max_length=8)
	password=models.CharField(max_length=200)
	confirm_password=models.CharField(max_length=200)
	def clean(self):
		cleaned_data = super(Akun, self).clean()
		password = cleaned_data.get("password")
		confirm_password = cleaned_data.get("confirm_password")
		if password != confirm_password:
			raise forms.ValidationError(
			"password and confirm_password does not match"
			)