from django.shortcuts import render
from .forms import Register_Form
from .models import Akun
def index(request):
    return render(request, 'base.html')
def account_save(request):
	form = Register_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['nama_akun'] = request.POST['nama_akun']
		response['email_akun'] = request.POST['email_akun']
		response['dob'] = request.POST['dob']
		akun_baru = Akun(nama_akun=response['nama_akun'], email_akun=response['email_akun'],
							  dob=response['dob'])
		akun_baru.save()
		return render(request, response)
	else:        
		return HttpResponseRedirect('/register/')
# Create your views here.
