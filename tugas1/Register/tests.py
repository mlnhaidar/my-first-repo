from django.test import TestCase
from .views import index
from .models import Akun
from datetime import datetime
def test_register_url_is_exist(self):
	response = Client().get('/register/')
	self.assertEqual(response.status_code, 200)
def test_register_using_index_func(self):
	found = resolve('/register/')
	self.assertEqual(found.func, index)
def test_model_can_create_new_akun(self):
	# Creating a new activity
	new_activity = Akun.objects.create(nama_akun='a', email_akun='b@gmail.com', dob=datetime.now(), password='asd')
	# Retrieving all available activity
	counting_all_available_akun = Akun.objects.all().count()
	self.assertEqual(counting_all_available_akun, 1)
# Create your tests here.
