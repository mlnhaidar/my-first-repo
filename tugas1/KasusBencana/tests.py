from django.test import TestCase
def test_kasus_bencana_url_is_exist(self):
	response = Client().get('/kasus-bencana/')
	self.assertEqual(response.status_code, 200)
def test_kasus_bencana_using_index_func(self):
	found = resolve('/kasus-bencana/')
	self.assertEqual(found.func, index)
# Create your tests here.
