"""tugas1e7 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.contrib import admin
from django.urls import re_path
from KasusBencana.views import index as index_hal1
urlpatterns = [
    re_path('admin/', admin.site.urls),
	re_path(r'^$', index_hal1, name='index'),
	re_path(r'^kasus-bencana/', include('KasusBencana.urls')),
	#re_path(r'^program-donasi/', include('ProgramDonasi.urls')),
    re_path(r'^ProgramDonasi/', include('ProgramDonasi.urls')),
	re_path(r'^register/', include('Register.urls')),
	re_path(r'^form-donasi/', include('FormDonasi.urls')),
]
