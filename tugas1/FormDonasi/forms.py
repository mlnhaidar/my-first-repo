from django import forms

from .models import IkutDonasi

class FormDonasi(forms.ModelForm):
	class Meta:
		model = IkutDonasi
		fields = [
			'nama_program',
			'nama_donatur',
			'email_donatur',
			'nominal',
			'anonym'
		]