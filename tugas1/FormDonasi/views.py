from django.shortcuts import render
from .models import IkutDonasi
from .forms import FormDonasi
response = {}
def index(request):
    return render(request, 'FormDonasi.html')
def submit_donasi(request):
	if (request.method == 'POST'):
		form_donasi = FormDonasi(request.POST or None)
		if (form.is_valid()):
			if (request.POST['anonym']):
				response['nama_donatur'] = 'Anonymous'
			else : 
				response['nama_donatur'] = request.POST['nama_donatur']
			response['anonym'] = request.POST['anonym']
			response['nama_program'] = request.POST['nama_program']
			response['email_donatur'] = request.POST['email_donatur']
			response['nominal'] = request.POST['nominal']

			ikut_donasi = IkutDonasi(nama_program=response['nama_program'],
						  			 nama_donatur=response['nama_donatur'],
						   			 email_donatur=response['email_donatur'],
						   			 nominal=response['nominal'], 
						   			 anonym=response['anonym'])

			ikut_donasi.save()

			return redirect('<<-----INDEX URL----->>')


def get_donation_info(request):
    return render(request, 'base.html', context)



# Create your views here.
