from django.test import TestCase
from .views import index
def test_form_donasi_url_is_exist(self):
	response = Client().get('/form-donasi/')
	self.assertEqual(response.status_code, 200)
def test_form_donasi_using_index_func(self):
	found = resolve('/form-donasi/')
	self.assertEqual(found.func, index)
##def test_model_can_create_new_akun(self):
	# Creating a new activity
##	new_activity = Todo.objects.create(title='mengerjakan lab ppw', description='mengerjakan lab_5 ppw')
# Create your tests here.