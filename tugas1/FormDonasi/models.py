from django.db import models

# Create your models here.

class IkutDonasi(models.Model):
	nama_program = models.CharField(max_length=48)
	nama_donatur = models.CharField(max_length=32)
	email_donatur = models.EmailField(max_length=64)
	nominal = models.DecimalField(max_digits=16, decimal_places=13)
	anonym = models.BooleanField()

