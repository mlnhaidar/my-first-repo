from django.test import TestCase
def test_program_donasi_url_is_exist(self):
	response = Client().get('/program-donasi/')
	self.assertEqual(response.status_code, 200)
def test_program_donasi_using_index_func(self):
	found = resolve('/program-donasi/')
	self.assertEqual(found.func, index)